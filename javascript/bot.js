var Bot = function (car, track, botName, botKey) {

  this.car = car;
  this.track = track;

  this.name = botName;
  this.key = botKey;

  this.gameTick = 0;
  this.tickQueue = [];

  this.gameInit = function (d) {
    this.car.init(botName);
    this.track.init(d.data);
  };

  this.gameStart = function (d) {
    this.gameTick = d.gameTick;
  };

  this.gameEnd = function (d) {
    console.log(JSON.stringify(d));
  };

  this.spawn = function (d) {
    this.car.spawned();
  };

  this.crash = function (d) {
    this.car.crashed();
  };

  this.log = function (throttle, speed, maxSpeed, angle, previousAngle, currentPositionOnPiece, acceleration, incurve, distanceToCurve, currentLane) {
    console.log("throttle: "+throttle+" || speed: "+speed+" || maxSpeed: "+maxSpeed+" || angle: "+angle+" || previousAngle: "+previousAngle+" || currentPositionOnPiece: "+currentPositionOnPiece+" || acceleration: "+acceleration+" || incurve: "+incurve+" || distanceToCurve: "+distanceToCurve+" || currentLane: "+currentLane);
  };

  this.drive = function (d) {
    this.gameTick = d.gameTick;
    var throttle = 1.0;
    var override = false;

    //find data for our car
    for (var i = d.data.length - 1; i >= 0; i--) {
      if(d.data[i].id.name == this.car.name){

        //update the track with new data
        this.track.update(d.data[i]);

        //update the car with new data & updated track
        this.car.update(d.data[i], this.track);

        //logic

        if (this.track.isCurrentPieceCurve) {
          //right turn
          if (this.car.previousAngle > 0) {
            if ((this.car.angle - this.car.previousAngle) < 2.0) {
              if (this.car.angle <= 52) {
                throttle = 1.0;
              }else{
                throttle = 0;
              }
            }
            if (this.car.angle - this.car.previousAngle > 2.0) {
              throttle = 0;
            }
          }
          //left turn
          if (this.car.previousAngle < 0) {
            var angle = (this.car.angle * -1);
            var previousAngle = (this.car.previousAngle * -1);
            if (angle >= 0 && previousAngle >= 0) {
              if ((angle - previousAngle) < 2.0) {
                if (angle <= 52) {
                  throttle = 1.0;
                }else{
                  throttle = 0;
                }
              }
              if (angle - previousAngle > 2.0) {
                throttle = 0;
              }
            }
          }
        }else{
          if (this.track.nextCurveAngle >= 45 || this.track.nextCurveAngle <= -45) {
            // if (this.car.angle > 2 || this.car.angle < -2) {
            //   throttle = 0;
            // }
            if (!this.track.isCurrentPieceCurve && this.track.distanceToCurve <= 75 && this.car.speed > 7.6) {
              throttle = 0;
            }
            if (!this.track.isCurrentPieceCurve && this.track.distanceToCurve <= 210 && this.car.speed > 9 ) {
              throttle = 0;
            }
          }
        }
        break;
      }
    }

    if (this.track.switchLane) {
      override = {"msgType": "switchLane", "data": this.track.switchLane, "gameTick": this.gameTick};
    };

    var speed = this.car.speed.toFixed(2);
    var maxSpeed = this.car.maxSpeed.toFixed(2);
    var angle = this.car.angle.toFixed(2);
    var previousAngle = this.car.previousAngle.toFixed(2);
    var currentPositionOnPiece = this.track.currentPositionOnPiece.toFixed(2);
    var acceleration = this.car.acceleration.toFixed(2);
    var distanceToCurve = this.track.distanceToCurve.toFixed(2);

    this.log(throttle, speed, maxSpeed, angle, previousAngle, currentPositionOnPiece, acceleration, this.track.isCurrentPieceCurve, distanceToCurve, this.track.currentLane);
    if (!override) {
      return {"msgType": "throttle", "data": throttle, "gameTick": this.gameTick}
    }else{
      return override;
    }
  };

  return this;
}

exports.Bot = Bot;