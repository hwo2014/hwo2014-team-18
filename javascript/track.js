var Track = function () {

  this.lanes = [];
  this.pieces = [];

  this.trackName = null;

  this.currentLane = 0;
  this.currentPiece = 0;
  this.nextPiece = 0;

  this.isCurrentPieceCurve = false;
  this.distanceToCurve = 0;
  this.currentPositionOnPiece = 0;
  this.lastPositionOnPiece = 0;
  this.nextCurveAngle = 0;
  this.switchLane = false;
  this.switching = false;

  this.init = function (data) {
    //setup the track
    this.laps = data.race.raceSession.laps;

    var tempPices = data.race.track.pieces;
    var tempPieces2 = data.race.track.pieces;
    this.pieces = tempPices.concat(tempPieces2);
    this.routeIndex = 0;

    console.log(this.pieces);

    this.lanes = data.race.track.lanes;
    this.trackName = data.race.track.name;

    console.log('Track init:done');
  };

  this.update = function (data) {
    //update data for the object ## this will update on every tick

    if (data.piecePosition.pieceIndex !== this.currentPiece.index) {
      this.switching = false;
    }

    this._currentPiece(this.pieces[data.piecePosition.pieceIndex], data.piecePosition.pieceIndex);
    this._nextPiece(this.pieces[data.piecePosition.pieceIndex+1]);
    this._lastPositionOnPiece(this.currentPositionOnPiece);
    this._currentPositionOnPiece(data.piecePosition.inPieceDistance);
    this._isCurrentPieceCurve(this.pieces[data.piecePosition.pieceIndex]);
    this._nextCurveAngle(data.piecePosition.pieceIndex);
    this._distanceToCurve(this.pieces[data.piecePosition.pieceIndex], data.piecePosition.pieceIndex);
    this._currentLane(data.piecePosition.lane.startLaneIndex);
    this._switchLane(data.piecePosition.pieceIndex);
  };

  // #### private functions used to update object properties ####
  this._currentPiece = function (piece, index) {
    this.currentPiece = piece;
    this.currentPiece.index = index;
  };

  this._nextPiece = function (piece) {
    this.nextPiece = piece;
  }

  this._currentLane = function (lane) {
    this.currentLane = lane;
  };

  this._lastPositionOnPiece = function (position) {
    // update last position on piece
    this.lastPositionOnPiece = position;
  };

  this._currentPositionOnPiece = function (position) {
    //update current position on piece
    this.currentPositionOnPiece = position;
  };

  this._isCurrentPieceCurve = function (piece) {
    //check if current piece is a curve
    if (piece.angle) {
      this.isCurrentPieceCurve = true;
    }else{
      this.isCurrentPieceCurve = false;
    }
  };

  this._nextCurveAngle = function (index) {
    for (var i = index+1; i < this.pieces.length; i++) {
      if (this.pieces[i].angle) {
        this.nextCurveAngle = this.pieces[i].angle;
        break;
      }
    }
  };

  this._distanceToCurve = function (currentPiece, index) {
    //calculate and update the distance to next curve
    var piecesBeforeCurve = [];
    var distance = 0;

    for (var a = index+1; a < this.pieces.length; a++) {
      if (this.pieces[a].angle) {
        for (var b = 0; b < piecesBeforeCurve.length; b++) {
          distance += piecesBeforeCurve[b].length;
        };
        if (currentPiece.length >= 0 && this.currentPositionOnPiece >= 0) {
          distance += (currentPiece.length - this.currentPositionOnPiece);
          this.distanceToCurve = distance;
        }else{
          this.distanceToCurve = 0;
        }
        break;
      }else{
        piecesBeforeCurve.push(this.pieces[a]);
      }
    };
  };

  this._switchLane = function (index) {
    var route = [{"lane":"0","switch":"Right"},{"lane":"1","switch":"Left"},{"lane":"false","switch":"false"},{"lane":"0","switch":"Right"},{"lane":"false","switch":"false"},{"lane":"false","switch":"false"},{"lane":"false","switch":"false"}];

    this.switchLane = false;
    if (this.nextPiece.switch && !this.switching) {
      for(var i=0; i < route.length; i++){
        if (i === this.routeIndex) {
          if (route[i].switch !== "false") {
            if (route[i].lane !== this.currentLane) {
              this.switchLane = route[i].switch;
            }
          }
          this.switching = index;
          console.log(route[i].switch);
          this.routeIndex += 1;
          if (this.routeIndex >= route.length) {
            this.routeIndex = 0;
          }
          break;
        }
      }
    }
  };

  this._shortestRoute = function () {
    var curves = [];
    for(var i = 0; i < this.pieces.length; i++){
      if (this.pieces[i].angle) {
        var curve = {
          "angle": null,
          "distance": null,
          "radius": null,
          "pieces": []
        }
        curve.pieces.push(this.pieces[i]);
      }
    }
  };

  this._nextCurveMaxSpeed = function () {
    return this._nextCurveMaxSpeed;
  };

  return this;
}

exports.Track = Track;