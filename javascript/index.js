var net = require("net");
var JSONStream = require('JSONStream');

var botModule = require('./bot.js');
var trackModule = require('./track.js');
var carModule = require('./car.js');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

//tracks
var germany = {"msgType": "joinRace", "data": { "botId": { "name": botName, "key": botKey }, "trackName": "germany", "carCount": 1 }}
var finland = {"msgType": "join", "data": { name: botName, key: botKey }}
client = net.connect(serverPort, serverHost, function() {
  return send(finland);
});

// Initialize bot
var car = new carModule.Car();
var track = new trackModule.Track();
var bot = new botModule.Bot(car, track, botName, botKey);

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    send(bot.drive(data));
  }
  else {
    if (data.msgType === 'join') {
      console.log('Joined');
    }
    else if (data.msgType === 'gameInit') {
      console.log('gameInit');
      bot.gameInit(data);
    }
    else if (data.msgType === 'gameStart') {
      console.log('gameStart');
      bot.gameStart(data);
    }
    else if (data.msgType === 'lapFinished') {
      console.log('lapFinished');
    }
    else if (data.msgType === 'crash') {
      console.log('crash');
      bot.crash(data);
    }
    else if (data.msgType === 'spawn') {
      console.log('spawn');
      bot.spawn(data);
    }
    else if (data.msgType === 'gameEnd') {
      console.log('gameEnd');
      bot.gameEnd(data);
    }
    else if (data.msgType === 'error') {
      console.log(JSON.stringify(data));
    }
    else{
      console.log(JSON.stringify(data));
    }

    send({
      msgType: "ping",
      data: {},
      "gameTick": bot.gameTick
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});





