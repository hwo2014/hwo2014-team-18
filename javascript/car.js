var Car = function () {

  this._data = null;

  this.name = null;
  this.color = null;

  this.maxSpeed = 0;
  this.maxAngle = 0;

  this.angle = 0;
  this.previousAngle = 0;
  this.speed = 0;
  this.acceleration = 0;

  this.init = function (carName) {
    // setup the car
    this.name = carName;
    console.log('Car init:done');
  };

  this.update = function (data, track) {
    //update data for the object
    this._data = data;

    var oldSpeed = this.speed;
    this._speed(track.lastPositionOnPiece, track.currentPositionOnPiece);
    var newSpeed = this.speed;

    this._acceleration(oldSpeed, newSpeed);
    this._angle(this._data.angle);
  };

  this.crashed = function () {

  };

  this.spawned = function () {

  };


  // #### private functions used to update object properties ####

  //calculate angle
  this._angle = function () {
    if (this._data.angle > this.maxAngle) {
      this.maxAngle = this._data.angle;
    }
    this.previousAngle = this.angle;
    this.angle = this._data.angle;
  };

  //calculate speed ( m/tick )
  this._speed = function (lastPosition, currentPosition) {
    if (currentPosition < lastPosition) {
      lastPosition = currentPosition;
    };
    var newSpeed = ((currentPosition - lastPosition) / 1);
    if (newSpeed > this.maxSpeed) {
      this.maxSpeed = newSpeed;
    };
    this.speed = newSpeed;
  };

  //calculate acceleration
  this._acceleration = function (oldSpeed, newSpeed) {
    this.acceleration = ((newSpeed - oldSpeed) / 1);
  };

  return this;
}

exports.Car = Car;